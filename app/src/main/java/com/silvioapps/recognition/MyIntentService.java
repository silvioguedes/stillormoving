package com.silvioapps.recognition;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

public class MyIntentService extends IntentService {
    private ResultReceiver resultReceiver = null;

    public MyIntentService(){
        super("");
    }

    @Override
    public void onCreate(){
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //...
        // If the intent contains an update
        if (ActivityRecognitionResult.hasResult(intent)) {
            // Get the update
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);

            DetectedActivity mostProbableActivity = result.getMostProbableActivity();

            List<DetectedActivity> mostProbableActivities = result.getProbableActivities();
            for(DetectedActivity detectedActivity : mostProbableActivities) {
                Log.i("TAG***", "detectedActivity " + detectedActivity.getType() + " confidence " + detectedActivity.getConfidence());
            }


            Intent localIntent = new Intent(Constants.BROADCAST_ACTION);
            localIntent.putExtra(Constants.ACTIVITY_EXTRA, mostProbableActivity);
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }
}
